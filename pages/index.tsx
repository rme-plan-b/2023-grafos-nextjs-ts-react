import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'

import axios from 'axios';
import React, { useEffect, useState } from 'react';

import dynamic from 'next/dynamic';

const NoSSRForceGraph = dynamic(() => import('./lib/NoSSRForceGraph'), {
  ssr: false
});

const inter = Inter({ subsets: ['latin'] })

export default function Home() {

  const [data, setData] = useState<any>([]);

  useEffect(() => {
    async function fetchData() {
      const res = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=20');
      setData(res.data.results);
    }
    fetchData();
  }, []);

  const myData2 = { 
    nodes: data.map((d: any) => ({ id: d.name })),
    links: data.map((d: any) => ({ source: 'bulbasaur', target: d.name })) 
  };
  console.log("myData2 ...... ", myData2);

  return (
    <>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <div className={styles.description}>
          <p>
            Ejemplo: &nbsp;
          </p>
        </div>

        <div className="h-screen">
          <NoSSRForceGraph graphData={ myData2 } />;
        </div>
        
      </main>
    </>
  )
}
